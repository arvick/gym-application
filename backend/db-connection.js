const mysql = require('mysql');

function connection(){
    const db = mysql.createConnection({
        host:'localhost',
        user:'angular_user',
        password:'root',
        database:'gym_app'
    });
    
    db.connect((err)=>{
        if(err){
            throw err;
        }
    
        console.log("connection established");
    });

    return db;
}


module.exports= connection;