const express = require('express');
const cors  =   require('cors');
const db = require('./db-connection');
const bodyParser    =   require('body-parser');
const routes = require('./routes.js');
const multer = require('multer');
const upload = multer();

const app = express();


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(upload.array());
app.use('/',  routes);

const PORT = process.env.PORT || 3000;

app.listen(PORT, ()=>{
    console.log(`listening at port ${PORT}`);
});


