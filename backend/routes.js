const express = require('express');
const dbConnection = require('./db-connection');
const bodyParser   = require('body-parser');
const router = express.Router();
const jwt   = require('jsonwebtoken');

const urlEncodedParser = bodyParser.urlencoded({ extended: false });

router.get('/api/', (req, res)=>{
    res.send('welcome user Home page');
});

const db = dbConnection();
var decodedToken="";
// varifying token

function varifyToken(req, res, next){
    let token = req.headers.authorization;

    jwt.verify(token,'desire',(err, tokenData)=>{
        if(err){
            return res.status(400).json({message:"unauthorized access"});
        }
        if(tokenData){
            decodedToken = tokenData;
            next();
        }
    });
}

router.post('/api/member', varifyToken, urlEncodedParser, (req, res)=>{
    // const postData = {name: "test", age: "43", email: "arvikc@gmail.com", mobile: "7676098864", password: "test"};
    const postData = req.body;
    // res.send("member page"+ postData);
    console.log(req.body);
    const sql = `insert into members_table SET ?`;
    db.query(sql, postData, (err, result)=>{
        if(err){
            throw err;
        }
        if(result.affectedRows ==1)
            res.send(`data inserted with id ${result.insertId}`);
        res.end();
    }); 
});


router.post('/api/adminLogin', urlEncodedParser, (req, res)=>{
    const sql = `select * from admin_table where username="${req.body.username}" and password="${req.body.password}"`;
    console.log(sql);
    db.query(sql,(err, result)=>{
        if(err) throw err;
        if(result.length>0)
        {
         let token = jwt.sign({username:req.body.username},'desire',{expiresIn:'3h'});
         res.status(200).json(token);
         
        }
        else
        res.send(result);
    });
})

router.get('/api/member-list', varifyToken, (req, res)=>{
    console.log(decodedToken.username);
    const sql = "select * from members_table";

    db.query(sql,(err, result)=>{
        if(err) throw err;

        res.send(result);
    });
});

router.post('/api/updateMember/:id', varifyToken, (req, res)=>{
    const sql = `update members_table set ? where id=?`;
    let data=[req.body, req.params.id];
    console.log(sql);
    db.query(sql, data, (err, result)=>{
        if(err) throw err;

        res.send(result);
    });
})


router.delete('/api/member/:id', (req, res)=>{
    const sql = `delete from members_table where id=${req.params.id}`;
    console.log(sql);
    db.query(sql, (err, result)=>{
        if(err){
            res.status(400).json("error in deleting record");
        }

        res.status(200).json("record deleted successfully");
    })
})


router.get('/api/member/:id', varifyToken, (req, res)=>{
    const sql= `select * from members_table where id=${req.params.id}`;

    db.query(sql, (err, result)=>{
        if(err) throw err;

        res.send(result);
    });
});

//add package
router.post('/api/package', varifyToken, (req, res)=>{
    let sql = `insert into packages_table SET ?`;

    db.query(sql, req.body, (err, result)=>{
        if(err) throw err;

        res.send(result);
    });
});

//Edit package
router.post('/api/packageEdit', varifyToken, (req, res)=>{
    let sql = "update packages_table SET ? where id=?";
    let data =[req.body, req.body.id];

    db.query(sql, data, (err, result)=>{
        if(err) throw err;

        res.send(result);
    });
});



router.get('/api/package', varifyToken, (req, res)=>{
    let sql = "select * from packages_table";

    db.query(sql,(err, result)=>{
        if(err) throw err;

        res.send(result);
    });
});


router.delete('/api/package/:id', varifyToken, (req, res)=>{
    let sql = `delete from packages_table where id=${req.params.id}`;

    db.query(sql, (err, result)=>{
        if(err){
            return res.status(400).json({"message":"error in deleting record"});
        }

        res.status(200).json({"message":"record deleted successfulyy"});

    })
})

module.exports = router;