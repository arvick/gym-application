-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 31, 2018 at 02:58 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gym_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_table`
--

CREATE TABLE IF NOT EXISTS `admin_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `pow` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_table`
--

INSERT INTO `admin_table` (`id`, `username`, `password`, `pow`) VALUES
(1, 'admin', 'password', 99);

-- --------------------------------------------------------

--
-- Table structure for table `members_table`
--

CREATE TABLE IF NOT EXISTS `members_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `members_table`
--

INSERT INTO `members_table` (`id`, `name`, `age`, `email`, `mobile`, `password`) VALUES
(1, '11112', 23, 'arvikc@gmail.coms', '7676098835', 'test'),
(2, 'test', 43, 'arvikc@gmail.com', '7676098864', 'test'),
(3, 'test', 43, 'arvikc@gmail.com', '7676098864', 'test'),
(4, 'Sahida Bano', 43, 'arvikc@gmail.com', '444444444', '44444444444'),
(5, 'Sahida Bano', 55, 'arvikddc@gmail.com', '444444', '55555555'),
(6, 'Abdul Rahim', 28, 'arvick99@gmail.com', '7676095448', '44444444'),
(7, 'test', 43, 'arvikc@gmail.com', '3344556678', 'test'),
(8, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', '3333'),
(9, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', 'test'),
(10, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', 'test'),
(11, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', 'test'),
(12, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', '3333333'),
(13, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', '4444444'),
(14, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', 'test'),
(15, 'Sahida Bano', 43, 'arvick99@gmail.com', '3344556678', 'test'),
(16, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', 'test'),
(17, 'Sahida Bano', 43, 'arvick99@gmail.com', '3344556678', '333333333'),
(18, 'Sahida Bano', 43, 'arvick99@gmail.com', '3344556678', '7777'),
(19, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', 'test'),
(20, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', '33333333333'),
(21, 'Sahida Bano', 43, 'arvikc@gmail.com', '3344556678', '444444444'),
(22, 'Sahida Bano', 43, 'arvikc@gmail.com', '7676098864', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `packages_table`
--

CREATE TABLE IF NOT EXISTS `packages_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `duration` int(11) NOT NULL,
  `amount` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `packages_table`
--

INSERT INTO `packages_table` (`id`, `name`, `duration`, `amount`) VALUES
(1, '1 year', 12, '5500'),
(2, '3 Months', 3, '3000'),
(3, '1 Month', 1, '2000'),
(4, '9 Months', 6, '7000'),
(5, 'test', 6, '333'),
(6, 'Sahida Bano', 6, ''),
(7, '2 months', 1, '2000');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
