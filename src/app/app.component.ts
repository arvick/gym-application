import { Component } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { AuthenticationService } from 'src/app/authentication.service';
import { Router, Event } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  isUserLoggedIn:boolean= false;

  constructor(private _authService:AuthenticationService, private _router:Router){
    this._router.events.subscribe((event:Event)=>{
      this.isUserLoggedIn=this._authService.isUserLoggedIn();
    })
  }

  ngOnInit(){
    this.isUserLoggedIn=this._authService.isUserLoggedIn();
    console.log(this.isUserLoggedIn);
  }
}
