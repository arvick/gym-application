import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient } from '@angular/common/http';

import {map} from 'rxjs/Operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  // isUserLoggedIn:boolean;
  constructor(private _cookieService:CookieService, private _httpClient:HttpClient) { 
  }

  logUserIn(post:any):any{
      // if(post.username=="admin" && post.password == "password"){
        let _url = 'http://localhost:3000/api/adminLogin';
        let status = false;
        return this._httpClient.post(_url, post);
        // return status;
      // }
  }

  isUserLoggedIn(){
    if(!this._cookieService.get('username'))
      return false;
    else
      return true;
  }

  logUserOut(){
    if(this._cookieService.get('username')){
      this._cookieService.delete('username');
      return true;
    }
  }
}
