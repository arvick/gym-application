import { Component, OnInit } from '@angular/core';
import { PackageService } from 'src/app/service/package.service';

@Component({
  selector: 'app-packages-list',
  templateUrl: './packages-list.component.html',
  styleUrls: ['./packages-list.component.scss']
})
export class PackagesListComponent implements OnInit {
  public _packageList:object;
  currentRecord:any;
  msg:string = "";
  details:boolean=false;
  constructor(private _service:PackageService) { }

  ngOnInit() {    
    this.getPackageList();
  }

  getPackageList(){
    this._service.getPackageList().subscribe(res=>{
      if(res){
        this._packageList = res;
      }
    }, (error)=>{
        console.log(error);
    });
  }

  showPackageDetails(record:any){
    console.log(record);
    if(!this.details)
      this.details = true;

    this.currentRecord = record;
  }


  removePackage(id){
    if(confirm("Are you sure to delete ?")){
      this._service.deletePackage(id).subscribe(res=>{
        if(res){
          this.msg = res.message;
          this.getPackageList();
        }
      }, error=>{
        this.msg = error.message;
      })
    }
  }

}
