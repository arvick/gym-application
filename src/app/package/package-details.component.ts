import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { PackageService } from 'src/app/service/package.service';
import { AfterViewInit, AfterContentChecked } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-package-details',
  templateUrl: './package-details.component.html',
  styleUrls: ['./package-details.component.scss']
})
export class PackageDetailsComponent implements OnInit, AfterContentChecked {
  @Input() record:any;
  message:string="";
  packageEdit:FormGroup;
  constructor(private _fb:FormBuilder, private _service:PackageService) {
    this.packageEdit=this._fb.group({
       'name':[null, Validators.required],
       'duration':[null, Validators.required],
       'amount':[null, Validators.required]
    })
   }

  ngOnInit() {
    // alert("initiated");
  }

  ngAfterContentChecked(){
    if(this.record){
      // this.packageEdit.controls['name'].setValue(this.record.name);
      // this.packageEdit.controls['duration'].setValue(this.record.duration);
      // this.packageEdit.controls['amount'].setValue(this.record.amount);
    }
  }

  editPackage(post:any){

    // post.set("test","test")
    post.id= this.record.id;

    console.dir(post);
    this._service.updatePackage(post).subscribe((res) => {
      if(res.affectedRows==1){
        this.message =`data updated ${res.affectedRows} row affected`;

        this._service.getPackageList();
      }
    },(error)=>{
      console.log(error);
    })
  }

}
