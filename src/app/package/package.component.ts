import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PackageService } from 'src/app/service/package.service';
import { error } from 'util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss']
})
export class PackageComponent implements OnInit {
  public pageAdd:boolean=true;
  public message:string="";
  public packageForm:FormGroup;
  constructor(private _fb:FormBuilder, private _service:PackageService, private _router:Router) { 
    this.packageForm=this._fb.group({
      'name':[null, Validators.required],
      'duration':['select', Validators.required],
      'amount':[null, Validators.required]
    });

    
  }

  ngOnInit() {
    this.packageForm.controls['duration'].setValue("select", {onlySelf: true});
  }

  submitPackage(post:FormData){
      this._service.submitPackage(post).subscribe((res) => {
        if(res.affectedRows==1){
          this.message =`data updated ${res.affectedRows} row affected`;

          setTimeout(()=>{
            this._router.navigate(['/package-list']);
          },500);
        }
      },(error)=>{
        console.log(error);
      })
  }

}
