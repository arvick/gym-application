import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IconHome, IconUsers, IconPackage, IconUser, IconList, IconDelete, IconEdit, IconLogOut} from 'angular-feather';

const icon =[
  IconHome,
  IconUsers,
  IconPackage,
  IconUser, 
  IconList,
  IconDelete,
  IconEdit,
  IconLogOut
]
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  exports: icon
})
export class IconsModule { }
