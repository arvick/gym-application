import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormGroup } from '@angular/forms';

import {MemberService} from './member.service';
import {CustomValidatorService} from '../custom-validator.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
  pageAdd:boolean=true;
  _id:string;
  ngForm:FormGroup;
  post:any;
  name:string='';
  age:number=0;
  email:string='';
  mobile:string='';
  password:string='';
  message:string='';

  constructor(private fb:FormBuilder, private service:MemberService, private _activatedRoute:ActivatedRoute, private _router:Router) {
    this._id = this._activatedRoute.snapshot.paramMap.get('id');
      this.ngForm = fb.group({
          'name':[null, Validators.required],
          'age': [null, Validators.required],
          'email':[null, Validators.compose([Validators.required, Validators.email])],
          'mobile':[null, Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10), CustomValidatorService.inputType])],
          'password':[null, Validators.required]
      })
   }

   submitMember(post:any){
    if(!this._id){
      this.service.submitMemberService(post).subscribe((msg) => {
        console.log(msg);
        this.message = msg;
      }, error => {
        console.log(error.error.text);
        this.message = error.error.text;});
        this.ngForm.reset();
    }else{
      this.service.updateMemberService(post, this._id).subscribe((msg) => {
        console.log(msg);
        if(msg.affectedRows==1){
          this.message =`data updated ${msg.affectedRows} row affected`;

          setTimeout(()=>{
            this._router.navigate(['/member-list']);
          },2000);
        }
      }, error => {
        console.log(error.error.text);
        this.message = error.error.text;});
    }        
        
   }

  ngOnInit() {    

    if(this._id){
        this.pageAdd=false;
        this.service.getMember(this._id).subscribe(data=>{
          if(data.length>0){
            this.ngForm.get('name').setValue(data[0].name);
            this.ngForm.get('age').setValue(data[0].age);
            this.ngForm.get('email').setValue(data[0].email);
            this.ngForm.get('mobile').setValue(data[0].mobile);
            this.ngForm.get('password').setValue(data[0].password);

          }
        })
    }
  }

}
