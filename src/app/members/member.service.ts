import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Observable } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class MemberService {

  constructor(private _http:HttpClient) {
   }

   
  submitMemberService(data:Object):any{
      let _url:string='http://localhost:3000/api/member';     
      return this._http.post(_url,data);
  }

  updateMemberService(data:Object, id:string):any{
    let _url:string = 'http://localhost:3000/api/updateMember/'+id;

    return this._http.post(_url, data);
  }
  getMemberList():any{
    let _url:string = 'http://localhost:3000/api/member-list';

    return this._http.get(_url);
  }

  getMember(id):any{
    let _url:string = 'http://localhost:3000/api/member/'+id;

    return this._http.get(_url);
  }
  
  removeMember(id):any{
    let url:string = 'http://localhost:3000/api/member/'+id;

    return this._http.delete(url);
  }

  private handleError(err:HttpErrorResponse){
    
            console.log(err);
    
            return Observable.throw(err.message);
        }
}
