import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataTablesModule} from 'angular-datatables';

import {MemberService} from './member.service';
import { error } from '@angular/compiler/src/util';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.scss']
})
export class MemberListComponent implements OnInit, OnDestroy {
  _memberList:any=[];
  msg:string="";
  pageTitle:string="Members List";
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {};
  constructor(private _service:MemberService) { }

  ngOnInit() {   
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
    this.getMemberList();
  }

  removeMember(id){
    if(confirm("Are you sure to delete ?"))
    {
        this._service.removeMember(id).subscribe(res=>{
          if(res){
            this.msg = res;
            this.getMemberList();
          }      
        },error=>{
          this.msg = error;
        })
      }
    }
    
    
    getMemberList(){
      this._service.getMemberList().subscribe((res)=>{
        this._memberList = res;
        this.dtTrigger.next();
      });
    }
    
    ngOnDestroy(): void {
      // Do not forget to unsubscribe the event
      this.dtTrigger.unsubscribe();
    }

}
