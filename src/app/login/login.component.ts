import { Component, OnInit } from '@angular/core';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import { AuthenticationService } from 'src/app/authentication.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  message:string='';
  messageType:string='';
  constructor(private fb:FormBuilder, private _authService:AuthenticationService, private _router:Router, private _cookieService:CookieService) {
    this.loginForm=fb.group({
      'username':[null, Validators.required],
      'password':[null, Validators.required]
    });
   }

  ngOnInit() {
  }

  logUserIn(post:any){
    // this._authService.logUserIn(post);
    // console.log(this._authService.logUserIn(post));
    this._authService.logUserIn(post).subscribe(data=>{
      let router = this._router;
      
        if(data.length > 0)
        { 
          localStorage.setItem("token",data.toString());
          this._cookieService.set('username', post.username);
          this.message="Welcome User "+post.username;
          this.messageType="success";
          setTimeout(function(){router.navigate(['member-list']);},800);
      }else{
        this.message=`user doesn't exist ${post.username}`;
        this.messageType="danger";
      }
    });
      
  }

}
