import { BrowserModule } from '@angular/platform-browser';
import { NgModule, OnInit } from '@angular/core';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IconsModule } from 'src/app/icons/icons.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MembersComponent } from './members/members.component';
import { PackageComponent } from './package/package.component';
import { OfferComponent } from './offer/offer.component';
import { AdminComponent } from './admin/admin.component';
import { CustomValidatorService } from 'src/app/custom-validator.service';
import { MemberListComponent } from './members/member-list.component';
import { DataTablesModule} from 'angular-datatables';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HeaderComponent } from './header/header.component';
import { AuthenticationService } from 'src/app/authentication.service';
import { CookieService} from 'ngx-cookie-service';
import { PackageService } from 'src/app/service/package.service';
import { PackagesListComponent } from './package/packages-list.component';
import { PackageDetailsComponent } from './package/package-details.component';
import { InterceptorTokenService } from 'src/app/service/interceptor-token.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    MembersComponent,
    PackageComponent,
    OfferComponent,
    AdminComponent,
    MemberListComponent,
    LoginComponent,
    NavbarComponent,
    HeaderComponent,
    PackagesListComponent,
    PackageDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsModule,
    ReactiveFormsModule, 
    HttpClientModule,
    DataTablesModule    
  ],
  providers: [
    CustomValidatorService,
    AuthenticationService, 
    CookieService, 
    PackageService,
    {
      provide:HTTP_INTERCEPTORS,
      useClass:InterceptorTokenService,
      multi:true
      
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule{

 }
