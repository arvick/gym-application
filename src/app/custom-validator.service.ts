import { Injectable } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

const regx = /[^0-9]/;
@Injectable({
  providedIn: 'root'
})

export class CustomValidatorService extends Validators{
  static inputType(input:FormControl){
    
            if(input.value && input.value.length > 0){
                
                const matches = input.value.match(regx);

                return matches && matches.length ? {invalidCharacter:matches} : null;
            }else{
              return null;
            }
          }
}
