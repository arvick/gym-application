import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import { MembersComponent } from 'src/app/members/members.component';
import { PackageComponent } from 'src/app/package/package.component';
import { OfferComponent } from 'src/app/offer/offer.component';
import { AdminComponent } from 'src/app/admin/admin.component';
import { MemberListComponent } from 'src/app/members/member-list.component';
import { LoginComponent } from 'src/app/login/login.component';
import { AuthguardGuard } from 'src/app/authguard.guard';
import { PackagesListComponent } from 'src/app/package/packages-list.component';

const routes: Routes = [
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'dashboard',
    canActivate:[AuthguardGuard],
    component: DashboardComponent
  },
  {
    path:'members',
    canActivate:[AuthguardGuard],
    component: MembersComponent
  },
  {
    path:'member/:id',
    canActivate:[AuthguardGuard],
    component: MembersComponent
  },
  {
    path:'member-list',
    canActivate:[AuthguardGuard],
    component:MemberListComponent
  },
  {
    path:'packages',
    canActivate:[AuthguardGuard],
    component: PackageComponent
  },
  {
    path:'package-list',
    canActivate:[AuthguardGuard],
    component: PackagesListComponent
  },
  {
    path:'offers',
    canActivate:[AuthguardGuard],
    component: OfferComponent
  },
  {
    path:'admin',
    canActivate:[AuthguardGuard],
    component: AdminComponent
  },
  {
    path:' ',
    redirectTo:'login'
  },
  {
    path:'**',
    redirectTo:'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
