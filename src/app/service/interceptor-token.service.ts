import { Injectable } from '@angular/core';
import { HttpInterceptor,HttpRequest,HttpHandler, HttpSentEvent, HttpHeaderResponse,HttpProgressEvent,HttpResponse, HttpUserEvent } from '@angular/common/http';
import {  } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class InterceptorTokenService implements HttpInterceptor{
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    
    const token:string=localStorage.getItem("token");

    if(token){
      const cloned= req.clone({
        headers:req.headers.set("Authorization", token)
      });

      return next.handle(cloned);
    }else{
      return next.handle(req);
    }
  }

  constructor() { }
}
