import { TestBed, inject } from '@angular/core/testing';

import { InterceptorTokenService } from './interceptor-token.service';

describe('InterceptorTokenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InterceptorTokenService]
    });
  });

  it('should be created', inject([InterceptorTokenService], (service: InterceptorTokenService) => {
    expect(service).toBeTruthy();
  }));
});
