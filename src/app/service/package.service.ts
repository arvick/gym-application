import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class PackageService {

  constructor(private _httpClient:HttpClient) { }


  public submitPackage(post:FormData):any{
    let _url:string='http://localhost:3000/api/package'; 
    return this._httpClient.post(_url, post);
  }

  public getPackageList():Observable<any>{
    let _url:string='http://localhost:3000/api/package'; 
    return this._httpClient.get(_url);
  }

  public updatePackage(post:FormData):Observable<any>{
    let _url:string="http://localhost:3000/api/packageEdit";
    return this._httpClient.post(_url, post);
  }


  public deletePackage(id):any{
    const _url = "http://localhost:3000/api/package/"+id;
    return this._httpClient.delete(_url);
  }
}
