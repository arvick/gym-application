import { Component, OnInit } from '@angular/core';
import { Event, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isUserLoggedIn:boolean = false;
  constructor(private _router:Router, private _authService:AuthenticationService) {
    this._router.events.subscribe((event:Event)=>{
      this.isUserLoggedIn=this._authService.isUserLoggedIn();
    })
   }

  ngOnInit() {
    this.isUserLoggedIn = this._authService.isUserLoggedIn();
  }

  logUserOut(){
      if(confirm('do you want to logout..?')){
        if(this._authService.logUserOut()){
          localStorage.removeItem('token');
          this._router.navigate(['login']);
        }
      }

  }

}
